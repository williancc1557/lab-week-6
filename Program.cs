﻿namespace teresina
{
    public class Program
    {
        public static void Main(string[] args)
        {
            List<int> items = [];
            for (int i = 0; i < 100; i++)
            {
                items.Add(i);
            }

            ThreadsGenerator.ProcessBatchSequentially(items);
            ThreadsGenerator.ProcessBatchInParallel(items);
        }
    }
}