using System.Diagnostics;

class ThreadsGenerator
{
    public static void ProcessBatchSequentially(List<int> items)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();

        foreach (int item in items)
        {
            ProcessItem(item);
        }

        stopwatch.Stop();
        Console.WriteLine($"Tempo total para processar em sequência: {stopwatch.ElapsedMilliseconds} ms");
    }

    private static void ProcessItem(int item)
    {
        Thread.Sleep(100);
        Console.WriteLine($"Processando item: {item}");
    }

    public static void ProcessBatchInParallel(List<int> items)
    {
        Stopwatch stopwatch = new();
        stopwatch.Start();
        Thread[] threads = new Thread[items.Count];

        for (int i = 0; i < items.Count; i++)
        {
            threads[i] = new Thread(() => ProcessItem(items[i]));
            threads[i].Start();
        }

        foreach (Thread thread in threads)
        {
            thread.Join();
        }

        stopwatch.Stop();
        Console.WriteLine($"Tempo total para processar em paralelo: {stopwatch.ElapsedMilliseconds} ms");
    }
}